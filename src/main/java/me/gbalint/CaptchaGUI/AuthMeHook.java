package me.gbalint.CaptchaGUI;

import fr.xephi.authme.api.v3.AuthMeApi;

import org.bukkit.entity.Player;

public class AuthMeHook {

    private AuthMeApi authMeApi = null;

    public void initializeAuthMeHook() {
        authMeApi = AuthMeApi.getInstance();
    }

    public void removeAuthMeHook() {
        authMeApi = null;
    }

    public boolean isHookActive() {
        return authMeApi != null;
    }

    public boolean isAuthenticated(Player name) {
        return authMeApi != null && authMeApi.isAuthenticated(name);
    }

}