package me.gbalint.CaptchaGUI;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.security.NoSuchAlgorithmException;

public class staffPinGUI {
    private static ItemStack logoutItem = GUIItems.logoutItem;

    private static ItemStack pinKey = GUIItems.pinKey;
    private static ItemStack okKey = GUIItems.okKey;
    private static ItemStack pinPlaceholder = GUIItems.pinPlaceholder;
    private static ItemStack clearKey = GUIItems.clearKey;
    //private static ItemStack placeholderItem = GUIItems.placeholderItem;

    private static CaptchaGUI plugin = CaptchaGUI.getInstance();
    public static Inventory makeGUI(Player player) {
        ItemStack misc;
        if (player.getName().equalsIgnoreCase("lacika9994")||
                player.getName().equalsIgnoreCase("ati99")) {
            misc = GUIItems.badWolf.clone();
        }
        else{
            misc = GUIItems.placeholderItem.clone();
        }
        Inventory GUI = Bukkit.createInventory(null, 54, plugin.getConfig().getString("staffinvname"));
        int j = 0;
        for (int i = 1; i < 10; i++) {
            pinKey.setAmount(i);
            GUI.setItem(j,pinKey);
            j++;
            if (i==3)
                j=9;
            else if (i==6)
                j=18;
        }
        GUI.setItem(31,okKey);
        GUI.setItem(8,logoutItem);
        GUI.setItem(4,clearKey);
        for (int i = 0; i < 54; i++) {
            if (GUI.getItem(i) == null) {
                GUI.setItem(i, misc);

            }
        }
        return GUI;
    }
    public static void handleGUI(DataSource data, Player player, ItemStack clicked, int slot, Inventory inventory) throws NoSuchAlgorithmException {
        if (clicked.isSimilar(okKey) && inventory.containsAtLeast(pinPlaceholder,plugin.getConfig().getInt("staff-pin-length",4))){

            Bukkit.getScheduler().runTaskAsynchronously(plugin,()->{
                String hash = plugin.storage.getHash(player);
                try {
                    if (Crypto.comparePassword(data.pin.toString(),hash)){
                        data.inCaptcha = false;
                        data.planned = true;
                        player.sendMessage((plugin.getConfig().getString("passedmsg")));
                        player.closeInventory();
                    }
                    else {
                        Bukkit.getScheduler().runTask(plugin, () -> player.kickPlayer(plugin.getConfig().getString("staff-kick-message")));
                    }
                } catch (NoSuchAlgorithmException e) {
                    Bukkit.getScheduler().runTask(plugin, () -> player.kickPlayer("Hash generation error"));
                    e.printStackTrace();
                }
            });

        }else if (clicked.isSimilar(pinKey) && data.pin.length() < plugin.getConfig().getInt("staff-pin-length",4)){
            data.pin.append(clicked.getAmount());
            pinPlaceholder.setAmount(clicked.getAmount());
            inventory.setItem( data.pin.length() + 44 ,pinPlaceholder);
            data.planned = true;
            player.openInventory(inventory);
        }else if (clicked.isSimilar(pinKey)) {
            data.pin = new StringBuilder();
            player.kickPlayer(plugin.getConfig().getString("staff-kick-message"));
        }else if (clicked.isSimilar(clearKey)){
            data.pin = new StringBuilder();
            ItemStack misc = inventory.getItem(44).clone();
            for (int i = 45; i < 54; i++) {
                inventory.setItem( i, misc);
            }
            data.planned = true;
            player.openInventory(inventory);
        }else if (clicked.isSimilar(GUIItems.placeholderItem) || clicked.isSimilar(GUIItems.badWolf) || clicked.isSimilar(pinPlaceholder)){
        }
    }
}
