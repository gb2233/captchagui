package me.gbalint.CaptchaGUI;

import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.concurrent.ConcurrentHashMap;

public class InventoryRunnable extends BukkitRunnable {

    private final Player player;
    private final boolean first;
    static ConcurrentHashMap<Player, Integer> tries = new ConcurrentHashMap<>();

    public InventoryRunnable(Player player,boolean first) {
        this.player = player;
        this.first = first;
        tries.put(this.player,10);
    }

    @Override
    public void run() {
        if (!player.isOnline()){
            tries.remove(player);
            this.cancel();
            return;
        }
        if(player.hasPermission("captchagui.staff") || player.hasPermission("captchagui.default")) {
            DataSource data = DataSource.getData(player);
            data.noPerm = false;
            CreateCaptcha.createCaptcha(player, first);
            tries.remove(player);
            this.cancel();
            return;
        }
        Integer t = tries.get(player);
        if (t<1)
            player.kickPlayer("Permission error");
        tries.put(player,t-1);
    }

}
