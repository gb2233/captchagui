package me.gbalint.CaptchaGUI;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;


public class CaptchaGUI extends JavaPlugin {
	
	private static CaptchaGUI plugin;
	public static AuthMeHook hook;

    private FileConfiguration data;
    private File pwdFile;
    private File configFile;
    private FileConfiguration config;
    public static Set<String> pwdPlayerUuidSet = ConcurrentHashMap.newKeySet();
    public AbstractDataAccess storage;

    @Override
    public void onEnable() {
    	plugin = this;
    	config = plugin.getConfig();

        config.addDefault("config-reloaded", "Konfigurációk újratöltve");
        config.addDefault("invname", "Kattints a gyémántra");
        config.addDefault("staffinvname", "Írd be a jelszavad");
        config.addDefault("clickmeitem", "DIAMOND");
        config.addDefault("clickmemeta", 0);
        config.addDefault("clickmename", "Katt ide");
        config.addDefault("lowcount", 3);
        config.addDefault("highcount", 5);
        config.addDefault("dontclickmeitem", "STAINED_GLASS_PANE");
        config.addDefault("dontclickmemeta", 14);
        config.addDefault("dontclickmename", "NE ide kattints");
        config.addDefault("passedmsg", "Sikeresen teljesítetted a captchát");
        config.addDefault("bypasspermission", "captchagui.bypass");
        config.addDefault("bypasscomplete", "Sikeresen megkerülted a captchát");
        config.addDefault("logoutitem", "DRAGON_EGG");
        config.addDefault("logoutitemmeta", 0);
        config.addDefault("logoutitemname", "Kilépés");
        config.addDefault("logout-kick-message", "Kilépés a szerverválasztóba (Dragon eggre kattintás)");
        config.addDefault("staff-kick-message", "Hibás biztonsági PIN");
        config.addDefault("staff-code-login", false);
        config.addDefault("staff-pin-length", 4);
        config.addDefault("staff-pin-changed", "A pin jelszavad megváltozott. Új jelszavad: ");
        config.addDefault("staff-pin-reset", " jelszava megváltozott. Új ideiglenes jelszava: ");
        config.addDefault("staff-pin-set-wrong-pin", "Hibás jelszó formátum!");

        config.addDefault("db.enable", false);
        config.addDefault("db.host", "localhost");
        config.addDefault("db.port", 3306);
        config.addDefault("db.name", "minecraft");
        config.addDefault("db.user", "root");
        config.addDefault("db.pass", "root");
        config.addDefault("db.table", "captchagui");

        config.options().copyDefaults(true);
        saveConfig();
        reloadPwdData();
    	hook = new AuthMeHook();
    	hook.initializeAuthMeHook();
    	if (config.getBoolean("db.enable",false)){
            try {
                storage = new DBHandler();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        else {
    	    storage = new FlatfileHandler();
        }
        this.getCommand("captchagui").setExecutor(new captchaCommand());
        getServer().getPluginManager().registerEvents(new PlayerListener(), this);

        new PlayerRunnable().runTaskTimer(plugin,20L, 20L);

    }

    @Override
    public void onDisable(){
    	hook.removeAuthMeHook();
    	if (storage instanceof DBHandler)
    	    ((DBHandler) storage).shutdown();
        plugin = null;
      }

    public void reloadPwdData() {
        saveDefaultConfig();
        data = YamlConfiguration.loadConfiguration(pwdFile);
    }
    public FileConfiguration getPwdConfig() {
        if (data == null) {
            reloadPwdData();
        }
        return data;
    }
    public void savePwdConfig() {
        if (data == null || pwdFile == null) {
            return;
        }
        try {
            getPwdConfig().save(pwdFile);
        } catch (IOException ex) {
            getLogger().log(Level.SEVERE, "Could not save config to " + pwdFile, ex);
        }
    }
    public void saveDefaultConfig() {
        if (pwdFile == null) {
            pwdFile = new File(getDataFolder(), "pins.yml");
        }
        if (!pwdFile.exists()) {
            this.saveResource("pins.yml", false);
        }
    }
    public static CaptchaGUI getInstance() {
    	return plugin;
    }
}
