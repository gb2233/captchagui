package me.gbalint.CaptchaGUI;

import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.Map;
import java.util.UUID;

public class FlatfileHandler extends AbstractDataAccess {
    public FlatfileHandler() {
        CaptchaGUI.pwdPlayerUuidSet = plugin.getPwdConfig().getConfigurationSection("users").getKeys(false);
    }

    @Override
    public void setData(Player player, String pin) throws NoSuchAlgorithmException {
        UUID pUuid = player.getUniqueId();
        plugin.getPwdConfig().set("users."+ pUuid.toString() +".hash",Crypto.computeHash(pin,Crypto.genSalt()));
        plugin.getPwdConfig().set("users."+ pUuid.toString() +".name", player.getName());
        plugin.savePwdConfig();
        CaptchaGUI.pwdPlayerUuidSet = plugin.getPwdConfig().getConfigurationSection("users").getKeys(false);
    }

    @Override
    public void setData(String pName, CommandSender sender) throws NoSuchAlgorithmException {
        String uuid = "";
        ConfigurationSection section = plugin.getPwdConfig().getConfigurationSection("users");
        for (Map.Entry<String, Object> entry : section.getValues(false).entrySet()){
            String name = section.getString(entry.getKey()+".name");
            if (pName.equalsIgnoreCase(name)){
                uuid = entry.getKey();
                break;
            }
        }
        String pin = "";
        if (!uuid.equals("")){
            pin = Crypto.genPin(plugin.getConfig().getInt("staff-pin-length",4));
            plugin.getPwdConfig().set("users."+ uuid + ".hash",Crypto.computeHash(pin,Crypto.genSalt()));
            if (PlayerListener.playerNotif.containsKey(uuid))
                PlayerListener.playerNotif.replace(uuid,pin);
            else
                PlayerListener.playerNotif.put(uuid,pin);
            plugin.savePwdConfig();
            CaptchaGUI.pwdPlayerUuidSet = plugin.getPwdConfig().getConfigurationSection("users").getKeys(false);
            sender.sendMessage(pName + plugin.getConfig().getString("staff-pin-reset") + pin);
        }

    }

    @Override
    public void setPin(String pName, CommandSender sender, String pin) throws NoSuchAlgorithmException, SQLException {

        String uuid = "";
        ConfigurationSection section = plugin.getPwdConfig().getConfigurationSection("users");
        for (Map.Entry<String, Object> entry : section.getValues(false).entrySet()){
            String name = section.getString(entry.getKey()+".name");
            if (pName.equalsIgnoreCase(name)){
                uuid = entry.getKey();
                break;
            }
        }
        if (!uuid.equals("")){
            Integer pinLen = plugin.getConfig().getInt("staff-pin-length",4);
            String regex = "[0-9]{" + pinLen + "}";
            if(pin.matches(regex)) {
                plugin.getPwdConfig().set("users." + uuid + ".hash", Crypto.computeHash(pin, Crypto.genSalt()));
                plugin.savePwdConfig();
                CaptchaGUI.pwdPlayerUuidSet = plugin.getPwdConfig().getConfigurationSection("users").getKeys(false);
                sender.sendMessage(pName + plugin.getConfig().getString("staff-pin-reset") + pin);
            }else {
                sender.sendMessage(plugin.getConfig().getString("staff-pin-set-wrong-pin"));
            }
        }

    }

    @Override
    public String getHash(Player player) {
        return plugin.getPwdConfig()
                .getConfigurationSection( "users."+player.getUniqueId().toString())
                .getString("hash");
    }
}
