package me.gbalint.CaptchaGUI;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.ConfigurationSection;

import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.Map;

public class captchaCommand  implements CommandExecutor {

    private static CaptchaGUI plugin = CaptchaGUI.getInstance();

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (cmd.getName().equalsIgnoreCase("captchagui") && sender.hasPermission("captchagui.edit")) {
            if (args.length == 0) {
                sender.sendMessage("§aCaptchaGUI ver" + plugin.getDescription().getVersion());
                sender.sendMessage("");
                sender.sendMessage("§a/cgui §7- §bto show this help menu");
                sender.sendMessage("§a/cgui reload §7- §bto reload the config file");
                sender.sendMessage("§a/cgui resetstaff <name> §7- §bgenerates new pin for a player on next login");
                sender.sendMessage("§a/cgui setpin <name> <pin> §7- §b sets a new pin for a player §4CONSOLE ONLY");
                sender.sendMessage("");
                return true;
            }else if (args.length == 1) {
                plugin.reloadConfig();
                plugin.reloadPwdData();
                sender.sendMessage(plugin.getConfig().getString("config-reloaded"));
            }else if (args.length == 2 && args[0].equalsIgnoreCase("resetstaff")) {
                try {
                    plugin.storage.setData(args[1],sender);
                    return true;
                } catch (NoSuchAlgorithmException | SQLException e) {
                    e.printStackTrace();
                }
            }else if ((sender instanceof ConsoleCommandSender) && args.length == 3 && args[0].equalsIgnoreCase("setpin")) {
                try {
                    plugin.storage.setPin(args[1],sender,args[2]);
                    return true;
                } catch (NoSuchAlgorithmException | SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }
}
