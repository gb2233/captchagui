package me.gbalint.CaptchaGUI;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class GUIItems {

    static ItemStack clickMeItem;
    static ItemStack dontClickMeItem;
    static ItemStack badWolf;
    static ItemStack logoutItem;
    static ItemStack placeholderItem;

    static ItemStack pinKey;
    static ItemStack okKey;
    static ItemStack pinPlaceholder;
    static ItemStack clearKey;
    private static CaptchaGUI plugin = CaptchaGUI.getInstance();

    static {
        clickMeItem = new ItemStack(Material.valueOf(plugin.getConfig().getString("clickmeitem")),
                1,
                (short) plugin.getConfig().getInt("clickmemeta"));
        ItemMeta clickMeMeta = clickMeItem.getItemMeta();
        clickMeMeta.setDisplayName(plugin.getConfig().getString("clickmename"));
        clickMeItem.setItemMeta(clickMeMeta);
        dontClickMeItem = new ItemStack(Material.valueOf(plugin.getConfig().getString("dontclickmeitem")),
                1,
                (short) plugin.getConfig().getInt("dontclickmemeta"));
        ItemMeta dontClickMeMeta = dontClickMeItem.getItemMeta();
        dontClickMeMeta.setDisplayName(plugin.getConfig().getString("dontclickmename"));
        dontClickMeItem.setItemMeta(dontClickMeMeta);

        logoutItem = new ItemStack(Material.valueOf(plugin.getConfig().getString("logoutitem")),
                1,
                (short) plugin.getConfig().getInt("logoutitemmeta"));
        ItemMeta logoutItemMeta = logoutItem.getItemMeta();
        logoutItemMeta.setDisplayName(plugin.getConfig().getString("logoutitemname"));
        logoutItem.setItemMeta(logoutItemMeta);

        badWolf = new ItemStack(Material.LAPIS_BLOCK, 1, (short) 0);
        ItemMeta bwmeta = badWolf.getItemMeta();
        bwmeta.setDisplayName("BAD WOLF");
        badWolf.setItemMeta(bwmeta);

        placeholderItem = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 7);

        ItemMeta placeholderItemItemMeta = placeholderItem.getItemMeta();
        placeholderItemItemMeta.setDisplayName("PIN");
        placeholderItem.setItemMeta(placeholderItemItemMeta);

        pinKey = new ItemStack(Material.WOOD_BUTTON, 1, (short) 0);
        ItemMeta pinKeyItemMeta = pinKey.getItemMeta();
        pinKeyItemMeta.setDisplayName("");
        pinKey.setItemMeta(pinKeyItemMeta);

        okKey = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 5);
        ItemMeta okKeyItemMeta = okKey.getItemMeta();
        okKeyItemMeta.setDisplayName("OK");
        okKey.setItemMeta(okKeyItemMeta);

        pinPlaceholder = new ItemStack(Material.BARRIER, 1, (short) 0);
        ItemMeta pinPlaceholderItemMeta = pinPlaceholder.getItemMeta();
        pinPlaceholderItemMeta.setDisplayName("PIN");
        pinPlaceholder.setItemMeta(pinPlaceholderItemMeta);

        clearKey = new ItemStack(Material.LAVA_BUCKET, 1, (short) 0);
        ItemMeta clearKeyItemMeta = clearKey.getItemMeta();
        clearKeyItemMeta.setDisplayName("CLEAR");
        clearKey.setItemMeta(clearKeyItemMeta);
    }

}
