package me.gbalint.CaptchaGUI;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

public class Crypto {

    private static final int HEX_MAX_INDEX = 16;
    private static final int LENGTH = 16;
    private static final char[] CHARS = "123456789abcdef0".toCharArray();
    private static final Random RANDOM = new SecureRandom();

/*
        */

    public static String genPin(int length){
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; ++i) {
            sb.append(CHARS[RANDOM.nextInt(9)]);
        }
        return sb.toString();
    }

    public static String genSalt(){
        StringBuilder sb = new StringBuilder(LENGTH);
        for (int i = 0; i < LENGTH; ++i) {
            sb.append(CHARS[RANDOM.nextInt(HEX_MAX_INDEX)]);
        }
        return sb.toString();
    }

    public static String computeHash(String password, String salt) throws NoSuchAlgorithmException {
        return "$SHA$" + salt + "$" + hash(hash(password) + salt);
    }

    public static boolean comparePassword(String password, String hash) throws NoSuchAlgorithmException {
        String[] line = hash.split("\\$");
        return line.length == 4 && isEqual(hash, computeHash(password, line[2]));
    }

    private static String hash(String message) throws NoSuchAlgorithmException {
        MessageDigest algorithm = MessageDigest.getInstance("SHA-256");
        algorithm.reset();
        algorithm.update(message.getBytes());
        byte[] digest = algorithm.digest();
        return String.format("%0" + (digest.length << 1) + "x", new BigInteger(1, digest));
    }

    private static boolean isEqual(String string1, String string2) {
        return MessageDigest.isEqual(
                string1.getBytes(StandardCharsets.UTF_8), string2.getBytes(StandardCharsets.UTF_8));
    }
}
