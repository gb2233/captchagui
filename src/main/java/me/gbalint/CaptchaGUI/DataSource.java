package me.gbalint.CaptchaGUI;

import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.entity.Player;

public class DataSource
{
    public static ConcurrentHashMap<Player, DataSource> datas;
    public boolean inCaptcha;
    public boolean planned;
    public boolean noPerm;
    public StringBuilder pin;
    
    public DataSource() {
        this.inCaptcha = false;
        this.planned = false;
        this.noPerm = true;
        pin = new StringBuilder();
    }
    
    public static DataSource getData(Player player) {
        if (player.isOnline() && !DataSource.datas.containsKey(player)) {
            DataSource.datas.put(player, new DataSource());
        }
        return DataSource.datas.get(player);
    }
    
    static {
        DataSource.datas = new ConcurrentHashMap<>();
    }
}