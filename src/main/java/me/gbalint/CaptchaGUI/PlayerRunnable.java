package me.gbalint.CaptchaGUI;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class PlayerRunnable extends BukkitRunnable {

    private static CaptchaGUI plugin = CaptchaGUI.getInstance();

    @Override
    public void run() {
        for(Player player: Bukkit.getOnlinePlayers()){
            if (!CaptchaGUI.hook.isAuthenticated(player)) {
                if(!CaptchaGUI.pwdPlayerUuidSet.isEmpty() &&
                        CaptchaGUI.pwdPlayerUuidSet.contains(player.getUniqueId().toString()) &&
                        (player.hasPermission("captchagui.staff") || player.isOp())){
                    if (player.getOpenInventory().getTitle().equalsIgnoreCase(plugin.getConfig().getString("invname"))) {
                        Bukkit.getScheduler().runTaskLater(plugin, player::closeInventory,1L);
                    }
                }
            }
        }
    }
}
