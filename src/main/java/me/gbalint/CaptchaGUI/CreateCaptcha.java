package me.gbalint.CaptchaGUI;

import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

public class CreateCaptcha
{
    public CreateCaptcha() {
    }

    public static void createCaptcha(Player player, boolean first) {
        if (!player.isOnline()) {
            return;
        }
        if (first) {
            DataSource data = DataSource.getData(player);
            data.inCaptcha = true;
		}
        Inventory GUI;
		if ((player.hasPermission("captchagui.staff") || player.isOp()) && CaptchaGUI.pwdPlayerUuidSet.contains(player.getUniqueId().toString())){
            GUI = staffPinGUI.makeGUI(player);
        }
        else{
            GUI = antiBotGUI.makeGUI(player);
        }
        player.openInventory(GUI);
    }
    
}
