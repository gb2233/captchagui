package me.gbalint.CaptchaGUI;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.concurrent.ConcurrentHashMap;

public class PlayerListener implements Listener{

    private static CaptchaGUI plugin = CaptchaGUI.getInstance();
    private static ItemStack logoutItem = GUIItems.logoutItem;
    static ConcurrentHashMap<String,String> playerNotif = new ConcurrentHashMap<>();



	public PlayerListener() {
		plugin = CaptchaGUI.getInstance();
	}
	
    /*@EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerInventoryOpen(InventoryOpenEvent event) {

    	if ((event.getPlayer() instanceof Player) && !CaptchaGUI.hook.isAuthenticated((Player)event.getPlayer())) {
    	    Player player = (Player)event.getPlayer();
    	    if(!CaptchaGUI.pwdPlayerUuidSet.isEmpty() &&
                    CaptchaGUI.pwdPlayerUuidSet.contains(player.getUniqueId().toString()) &&
                    (player.hasPermission("captchagui.staff") || player.isOp())){
                if (event.getInventory().getName().equalsIgnoreCase(plugin.getConfig().getString("invname"))) {
                    event.setCancelled(true);
                    event.getPlayer().closeInventory();
                }
            }
		}
    }*/

    @EventHandler
    public void onAuthed(fr.xephi.authme.events.LoginEvent event) throws NoSuchAlgorithmException, SQLException {
	    Player player = event.getPlayer();
        if ((playerNotif.keySet().contains(player.getUniqueId().toString())) ||
                (player.hasPermission("captchagui.staff") &&
                        !CaptchaGUI.pwdPlayerUuidSet.contains(player.getUniqueId().toString()))) {
            String pin = Crypto.genPin(plugin.getConfig().getInt("staff-pin-length",4));
            plugin.storage.setData(player,pin);
            player.sendMessage(plugin.getConfig().getString("staff-pin-changed") + pin);
            player.sendTitle(plugin.getConfig().getString("staff-pin-changed"),
                    pin, 10, 200, 10);
            playerNotif.remove(player.getUniqueId().toString());
	    }

    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerInventoryClick(InventoryClickEvent event) throws NoSuchAlgorithmException {
        Player player = (Player)event.getWhoClicked();
    	if (player != null && !CaptchaGUI.hook.isAuthenticated(player)) {
            DataSource data = DataSource.getData(player);
            event.setCancelled(true);
            if (data.inCaptcha) {
                if (event.getCurrentItem() != null) {
                    if (event.getCurrentItem().isSimilar(logoutItem))
                        player.kickPlayer(plugin.getConfig().getString("logout-kick-message"));
                    else if (!CaptchaGUI.pwdPlayerUuidSet.isEmpty() &&
                            CaptchaGUI.pwdPlayerUuidSet.contains(player.getUniqueId().toString()) &&
                            (player.hasPermission("captchagui.staff") || player.isOp())){
                        staffPinGUI.handleGUI(data,
                                player,
                                event.getCurrentItem(),
                                event.getSlot(),
                                event.getInventory());
                    }
                    else{
                        antiBotGUI.handleGUI(data,
                            player,
                            event.getCurrentItem(),
                            event.getSlot(),
                            event.getInventory());
                    }

                }
                else {
                    player.closeInventory();
                }
            }
    	}
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onInventoryCloseEvent(InventoryCloseEvent event) {
        if (event.getPlayer() instanceof Player) {
            if (event.getPlayer().hasMetadata("NPC")) {
                return;
            }
            Player player = (Player)event.getPlayer();
            if (event.getInventory() != null) {
                if (DataSource.getData(player).inCaptcha) {
                    if (DataSource.getData(player).planned) {
                    	DataSource.getData(player).planned = false;
                        return;
                    }
                    //new InventoryRunnable(player,false).runTaskTimer(plugin, 1L,20L);
                    new InventoryRunnable(player,false).runTaskLater(plugin, 1L);
                }
            }
        }
    }
    
    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerCommandPreprocess(PlayerCommandPreprocessEvent event) {
    	if (event.getPlayer() != null && (DataSource.getData(event.getPlayer()).inCaptcha || DataSource.getData(event.getPlayer()).noPerm)) {
    		event.setCancelled(true);
    	}
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerJoinEvent(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        if (player.hasPermission(plugin.getConfig().getString("bypasspermission"))) {
            player.sendMessage(plugin.getConfig().getString("bypasscomplete"));
            return;
        }
        new InventoryRunnable(player,true).runTaskTimer(plugin, 1L,20L);
        //new InventoryRunnable(player,true).runTaskLater(plugin, 1L);
    }
    @EventHandler(priority =  EventPriority.LOWEST)
    public  void onPlayerLeaveEvent(PlayerQuitEvent event){
        DataSource.datas.replace(event.getPlayer(), new DataSource());
    }

}
