package me.gbalint.CaptchaGUI;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

public abstract class AbstractDataAccess {
    static CaptchaGUI plugin = CaptchaGUI.getInstance();

    public abstract void setData(Player player, String pin) throws NoSuchAlgorithmException, SQLException;
    public abstract void setData(String pName, CommandSender sender) throws NoSuchAlgorithmException, SQLException;
    public abstract void setPin(String pName, CommandSender sender, String pin) throws NoSuchAlgorithmException, SQLException;
    public abstract String getHash(Player player);
}
