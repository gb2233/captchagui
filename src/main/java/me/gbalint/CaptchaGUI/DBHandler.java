package me.gbalint.CaptchaGUI;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.security.NoSuchAlgorithmException;
import java.sql.*;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class DBHandler extends AbstractDataAccess {
    private static HikariDataSource ds;
    private static String INSERT_OR_UPDATE_USER_DATA;
    private static String UPDATE_USER_HASH;
    private static String SELECT_UUID_FROM_NAME;
    private static String SELECT_USER_HASH;
    private static String SELECT_ALL_USER;
    private static String tableName;

    static{
        plugin = CaptchaGUI.getInstance();
        tableName = plugin.getConfig().getString("db.table","captchagui");

        INSERT_OR_UPDATE_USER_DATA = "INSERT INTO `" + tableName + "` (uuid,name,hash) VALUES (?,?,?) ON DUPLICATE KEY UPDATE uuid=VALUES(uuid), name=VALUES(name), hash=VALUES(hash);";
        SELECT_UUID_FROM_NAME = "SELECT uuid FROM `" + tableName + "` WHERE name=?";
        UPDATE_USER_HASH = "UPDATE `" + tableName + "` SET hash=? WHERE uuid=?";
        SELECT_USER_HASH = "SELECT `hash` FROM `" + tableName + "` WHERE uuid=?";
        SELECT_ALL_USER = "SELECT `uuid` FROM `" + tableName + "`";

        HikariConfig config = new HikariConfig();
        config.setPoolName("captchagui");
        config.setJdbcUrl("jdbc:mysql://" +
                        plugin.getConfig().getString("db.host","localhost") +
                        ":" +
                        plugin.getConfig().getInt("db.port",3306) +
                        "/" +
                        plugin.getConfig().getString("db.name","minecraft") +
                        "?useSSL=false"
                        );
        config.setUsername(plugin.getConfig().getString("db.user","root"));
        config.setPassword(plugin.getConfig().getString("db.pass","root"));
        //config.setDriverClassName("com.mysql.jdbc.jdbc2.optional.MysqlDataSource");
        config.setDriverClassName("com.mysql.jdbc.Driver");
        config.addDataSourceProperty("cachePrepStmts", "true");
        config.addDataSourceProperty("alwaysSendSetIsolation", "false");
        config.addDataSourceProperty("cacheServerConfiguration", "true");
        config.addDataSourceProperty("elideSetAutoCommits", "true");
        config.addDataSourceProperty("useLocalSessionState", "true");

        config.addDataSourceProperty("useServerPrepStmts", "true");
        config.addDataSourceProperty("prepStmtCacheSize", "250");
        config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");

        config.setMaximumPoolSize(5);
        config.setMinimumIdle(10);
        config.setMaxLifetime(1800000);
        config.setConnectionTimeout(5000);

        config.setLeakDetectionThreshold(TimeUnit.SECONDS.toMillis(10)); // 10000
        config.setInitializationFailTimeout(-1);

        ds = new HikariDataSource(config);
    }

    public DBHandler() throws SQLException {
        if (!tableExists(tableName)){
            Connection connection = getConnection();
            Statement s = connection.createStatement(); // "`id` INT AUTO_INCREMENT NOT NULL," +
                String statement = "CREATE TABLE `" + tableName + "` (" +
                        "`uuid` VARCHAR(36) NOT NULL," +
                        "`name` VARCHAR(16) NOT NULL," +
                        "`hash` VARCHAR(86) NOT NULL," +
                        " PRIMARY KEY (`uuid`)\n" +
                        ") DEFAULT CHARSET = utf8;";
                String statement2 = "CREATE INDEX `uuids` ON `" + tableName + "` (`uuid`);";
                String statement3 = "CREATE INDEX `names` ON `" + tableName + "` (`name`);";
                s.execute(statement);
                s.execute(statement2);
                s.execute(statement3);
                s.close();
                connection.close();
        }
        CaptchaGUI.pwdPlayerUuidSet.addAll(getUuids());

    }

    private Connection getConnection() throws SQLException {
        Connection connection = ds.getConnection();
        if (connection == null) {
            throw new SQLException("Unable to get a connection from the pool.");
        }
        return connection;
    }

    @Override
    public void setData(Player player, String pin){
        Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
            UUID pUuid = player.getUniqueId();
            try {
                Connection connection = getConnection();
                PreparedStatement ps = connection.prepareStatement(INSERT_OR_UPDATE_USER_DATA); // "`id` INT AUTO_INCREMENT NOT NULL," +
                ps.setString(1,pUuid.toString());
                ps.setString(2,player.getName());
                ps.setString(3,Crypto.computeHash(pin,Crypto.genSalt()));
                ps.executeUpdate();
                CaptchaGUI.pwdPlayerUuidSet.add(pUuid.toString());
                ps.close();
                connection.close();
            } catch (SQLException | NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void setData(String pName, CommandSender sender){
        Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
            try {
                Connection connection = getConnection();
                PreparedStatement ps = connection.prepareStatement(SELECT_UUID_FROM_NAME); // "`id` INT AUTO_INCREMENT NOT NULL," +
                ps.setString(1,pName);
                String uuid = "";
                ResultSet rs = ps.executeQuery();
                if (rs.first()) {
                    uuid = rs.getString("uuid");
                }

                if (!uuid.equals("")){
                    String pin = Crypto.genPin(plugin.getConfig().getInt("staff-pin-length",4));
                    generate(pin, connection, uuid);

                    if (PlayerListener.playerNotif.containsKey(uuid))
                        PlayerListener.playerNotif.replace(uuid,pin);
                    else
                        PlayerListener.playerNotif.put(uuid,pin);
                    CaptchaGUI.pwdPlayerUuidSet.add(uuid);
                    sender.sendMessage(pName + plugin.getConfig().getString("staff-pin-reset") + pin);
                }
                rs.close();
                ps.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void setPin(String pName, CommandSender sender, String pin) {
        Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
            try {
                Connection connection = getConnection();
                PreparedStatement ps = connection.prepareStatement(SELECT_UUID_FROM_NAME);// "`id` INT AUTO_INCREMENT NOT NULL," +
                ps.setString(1,pName);
                String uuid = "";
                ResultSet rs = ps.executeQuery();
                    if (rs.first()) {
                        uuid = rs.getString("uuid");
                    }

                if (!uuid.equals("")){
                    Integer pinLen = plugin.getConfig().getInt("staff-pin-length",4);
                    String regex = "[0-9]{" + pinLen + "}";
                    if(pin.matches(regex)){
                        generate(pin, connection, uuid);
                        CaptchaGUI.pwdPlayerUuidSet.add(uuid);
                        sender.sendMessage(pName + plugin.getConfig().getString("staff-pin-reset") + pin);
                    }else{
                        sender.sendMessage(plugin.getConfig().getString("staff-pin-set-wrong-pin"));
                    }

                }
                rs.close();
                ps.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    private void generate(String pin, Connection con, String uuid) throws SQLException {
        try {
            PreparedStatement ps2 = con.prepareStatement(UPDATE_USER_HASH);
            ps2.setString(1,Crypto.computeHash(pin,Crypto.genSalt()));
            ps2.setString(2,uuid);
            ps2.executeUpdate();
            ps2.close();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getHash(Player player) {
        String hash="";
        try {
            Connection connection = getConnection();
            PreparedStatement ps = connection.prepareStatement(SELECT_USER_HASH);
            ps.setString(1,player.getUniqueId().toString());
            ResultSet rs = ps.executeQuery();
            if (rs.first()) {
                hash = rs.getString("hash");
            }
            rs.close();
            ps.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return hash;
    }

    private boolean tableExists(String table) throws SQLException {
        boolean ex = false;
        Connection connection = getConnection();
        ResultSet rs = connection.getMetaData().getTables(null, null, "%", null);
        while (rs.next()) {
            if (rs.getString(3).equalsIgnoreCase(table)) {
                ex = true;
            }
        }
        rs.close();
        connection.close();
        return ex;
    }

    private Set<String> getUuids(){
        Set<String> uuids = new HashSet<>();
        try {
            Connection connection = getConnection();
            PreparedStatement ps = connection.prepareStatement(SELECT_ALL_USER);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                uuids.add(rs.getString("uuid"));
            }
            rs.close();
            ps.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return uuids;
    }

    public void shutdown() {
        if (ds != null) {
            try {
                ds.getConnection().close();
                ds.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}