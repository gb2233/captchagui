package me.gbalint.CaptchaGUI;

import java.util.concurrent.ThreadLocalRandom;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;


public class antiBotGUI {
    //private static antiBotGUI cgui;
    //private Inventory GUI;

	private static CaptchaGUI plugin = CaptchaGUI.getInstance();
    private static int low;
    private static int high;
    private static ItemStack clickMe = GUIItems.clickMeItem;
    private static ItemStack dontClickMe = GUIItems.dontClickMeItem;
    private static ItemStack logoutItem = GUIItems.logoutItem;
    private static ItemStack badwolf = GUIItems.badWolf;

    static{
        low = plugin.getConfig().getInt("lowcount");
        high = plugin.getConfig().getInt("highcount") + 1;
    }

    public antiBotGUI() {
    }

    public static Inventory makeGUI(Player player) {
        Inventory GUI = Bukkit.createInventory(null, 54, plugin.getConfig().getString("invname"));
        GUI.setItem(0, logoutItem);
        for (int i = 1; i < ThreadLocalRandom.current().nextInt(low, high); i++) {
            int toClick = ThreadLocalRandom.current().nextInt(54);
            if (GUI.getItem(toClick) != null) {
            	i--;
            }
            else {
                GUI.setItem(toClick, clickMe);
            }
        }
        ItemStack misc;
        if (player.getName().equalsIgnoreCase("lacika9994")||
                player.getName().equalsIgnoreCase("ati99"))
            misc = badwolf.clone();
        else
            misc = dontClickMe.clone();
        for (int j = 1; j < 54; ++j) {
            if (GUI.getItem(j) == null) {
                    GUI.setItem(j, misc);

            }
        }
        return GUI;
    }
    public static void handleGUI(DataSource data, Player player, ItemStack clicked, int slot, Inventory inventory) {
        if (clicked.isSimilar(clickMe)) {
            inventory.setItem(slot, dontClickMe);
            data.planned = true;
            player.openInventory(inventory);
            if (!inventory.contains(clickMe)) {
                data.inCaptcha = false;
                player.closeInventory();
                player.sendMessage((plugin.getConfig().getString("passedmsg")));
            }
            return;
        }
        player.closeInventory();

    }
 
}
